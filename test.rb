require 'capybara'
require 'capybara/dsl'
require 'minitest/autorun'
require 'test/unit'

Capybara.run_server = false
Capybara.current_driver = :selenium
Capybara.app_host = 'http://187.216.145.50:42128'
Capybara.ignore_hidden_elements = false

class TestCases < Test::Unit::TestCase
  include Capybara::DSL

  def got_to_alerts
  end

  def go_to_inputs
    visit('/Inputs')
  end

  def test_one
    go_to_inputs

    fill_in('Text Input', with: 'Contenido')

    fill_in('Text Area', with: 'Contenido Text Area')

    select('Ontario', from: 'Select')

    select('Nunavut', from: 'Multi Select')
    select('Yukon', from: 'Multi Select')
    select('Québec', from: 'Multi Select')
  end

  def test_two
    go_to_inputs
    click_button('Input Button')
  end

  def test_three
    visit('/')
    assert page.has_content?('Inputs')
  
    go_to_inputs
    assert page.has_content?('Input Controls Testbed')
    assert page.has_content?('Text Input')
  end
end


